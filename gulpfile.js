const gulp = require("gulp");
const sass = require("gulp-sass")(require("sass"));
const del = require("del");
const minify = require("gulp-minify");
const minifyjs = require("gulp-js-minify");
const cleanCSS = require("gulp-clean-css");
const autoprefixer = require("gulp-autoprefixer");
const concat = require("gulp-concat");
const imagemin = require("gulp-imagemin");
const browserSync = require("browser-sync").create();
const reload = browserSync.reload;

gulp.task("prepare-html", (cb) => {
  gulp.src("src/*.html").pipe(gulp.dest("dist"));
  cb();
});

gulp.task("prepare-css", (cb) => {
  gulp
    .src("./src/scss/**/*.{scss, css}")
    .pipe(sass().on("error", sass.logError))
    .pipe(
      autoprefixer({
        cascade: false,
      })
    )
    .pipe(cleanCSS({ compatibility: "ie8" }))
    .pipe(minify())
    .pipe(gulp.dest("./dist/css/"));
  cb();
});

gulp.task("prepare-js", (cb) => {
  gulp
    .src("./src/js/**/*.js")
    .pipe(concat("script.js"))
    .pipe(minifyjs())
    .pipe(gulp.dest("./dist/js/"));
  cb();
});

gulp.task("prepare-img", (cb) => {
  gulp.src("./src/img/*.png").pipe(imagemin()).pipe(gulp.dest("./dist/img/"));
  cb();
});

exports.watch = function watch() {
  gulp.watch("./src/scss/**/*.scss", gulp.series("prepare-css"));
};

gulp.task("clean", (cb) => {
  del("dist");
  cb();
});

gulp.task("serve", function () {
  browserSync.init({
    server: "./dist",
  });

  gulp.watch("./src/scss/**/*.scss", gulp.series("prepare-css"));
  gulp.watch("dist").on("change", reload);
});

exports.clean = gulp.series("clean");
exports.serve = gulp.series("serve");
exports.build = gulp.parallel(
  "prepare-html",
  "prepare-css",
  "prepare-js",
  "prepare-img"
);
