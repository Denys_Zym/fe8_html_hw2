const button = document.querySelector("button");
const menu = document.querySelector(".nav-section__menu");

button.addEventListener("click", () => {
  button.classList.toggle("burger-btn");
  button.classList.toggle("cancel-btn");
  const activeClass = button.classList.contains("cancel-btn")
    ? "open-menu"
    : "close-menu";
  const removeClass = button.classList.contains("cancel-btn")
    ? "close-menu"
    : "open-menu";
  menu.classList.remove(removeClass);
  menu.classList.add(activeClass);
});
