const tab = document.querySelector(".nav-section__menu");
const tabs = document.querySelectorAll(".nav-section__menu-title");

tab.addEventListener("click", (event) => {
  tabs.forEach((i) => {
    if (event.target === i) {
      event.target.classList.add("title--active");
    } else {
      i.classList.remove("title--active");
    }
  });
});
